
resource "aws_route53_zone" "zone" {
  name         = "bieber-demo.tk"
}

resource "aws_route53_record" "root" {
  zone_id = aws_route53_zone.zone.zone_id
  name    = "api.bieber-demo.tk"
  type    = "A"
  alias {
    name                   = aws_lb.lb.dns_name
    zone_id                = aws_lb.lb.zone_id
    evaluate_target_health = true
  }
}
resource "aws_route53_record" "www" {
  zone_id = aws_route53_zone.zone.zone_id
  name    = "www.bieber-demo.tk"
  type    = "A"
  alias {
    name                   = aws_cloudfront_distribution.s3_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.s3_distribution.hosted_zone_id
    evaluate_target_health = true
  }
}



resource "aws_acm_certificate" "cert" {
  domain_name       = "*.bieber-demo.tk"
  validation_method = "DNS"

  tags = {
    Name = "bieber-cert"
  }
}
