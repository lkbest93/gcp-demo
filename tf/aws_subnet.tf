

resource "aws_subnet" "private" {
  vpc_id = aws_vpc.vpc.id
  cidr_block       = "10.0.1.0/24"
  availability_zone = "ap-northeast-2a"
   tags = {
    Name = "private"
  }
}

resource "aws_subnet" "public" {
  vpc_id = aws_vpc.vpc.id
  cidr_block       = "10.0.0.0/24"
  availability_zone = "ap-northeast-2a"
   tags = {
    Name = "public"
  }
}

resource "aws_subnet" "private_c" {
  vpc_id = aws_vpc.vpc.id
  cidr_block       = "10.0.2.0/24"
  availability_zone = "ap-northeast-2c"
   tags = {
    Name = "private_c"
  }
}

resource "aws_subnet" "public_c" {
  vpc_id = aws_vpc.vpc.id
  cidr_block       = "10.0.3.0/24"
  availability_zone = "ap-northeast-2c"
   tags = {
    Name = "public_c"
  }
}