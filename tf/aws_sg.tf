resource "aws_security_group" "allow_vpc_inbound" {
  name        = "allow_tcp_from_vpc"
  description = "Allow all inbound traffic from vpc"
  vpc_id      = aws_vpc.vpc.id
  ingress {
    description      = "TCP from VPC"
    from_port        = 0
    to_port          = 65535
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.vpc.cidr_block]
  }
  tags = {
    Name = "allow_tcp_from_vpc"
    
  }
}
resource "aws_security_group" "allow_vpc_outbound" {
  name        = "allow_tcp_to_vpc"
  description = "Allow all outbound traffic from vpc"
  vpc_id      = aws_vpc.vpc.id
  egress {
    description      = "TCP to VPC"
    from_port        = 0
    to_port          = 65535
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.vpc.cidr_block]
  }
  tags = {
    Name = "allow_tcp_to_vpc"
    
  }
}
resource "aws_security_group" "allow_outbound" {
  name        = "allow_tcp_to_all"
  description = "Allow all outbound traffic to all"
  vpc_id      = aws_vpc.vpc.id
  egress {
    description      = "TCP to all"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  tags = {
    Name = "allow_outbound"
    
  }
}
resource "aws_security_group" "allow_my_ssh_inbound" {
  name        = "allow_ssh_from_me"
  description = "Allow my SSH inbound traffic"
  vpc_id      = aws_vpc.vpc.id
  ingress {
    description      = "SSH from me"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["27.122.140.10/32"]  
  }
  
  tags = {
    Name = "allow_ssh_from_me"
  }
}

resource "aws_security_group" "allow_http_https_inbound" {
  name        = "allow_http_https_inbound"
  description = "Allow 80 443 inbound traffic"
  vpc_id      = aws_vpc.vpc.id
  ingress {
    description      = "allow HTTP inbound"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]  
  }
  ingress {
    description      = "allow HTTPS inbound"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]  
  }
  tags = {
    Name = "allow_http_https_inbound"
  }
}

resource "aws_security_group" "allow_my_inbound" {
  name        = "allow_from_me"
  description = "Allow my inbound traffic"
  vpc_id      = aws_vpc.vpc.id
  ingress {
    description      = " from me"
    from_port        = 0
    to_port          = 65535
    protocol         = "tcp"
    cidr_blocks      = ["27.122.140.10/32"]  
  }
  
  tags = {
    Name = "allow_ssh_from_me"
  }
}