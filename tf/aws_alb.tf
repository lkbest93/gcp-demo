
resource "aws_lb" "lb" { 
  name               = "bieber-lb"
  internal           = false 
  load_balancer_type = "application"
  security_groups = [ 
      aws_security_group.allow_http_https_inbound.id, 
      aws_security_group.allow_vpc_inbound.id,
      aws_security_group.allow_outbound.id
  ]
  subnets            = [aws_subnet.public.id,aws_subnet.public_c.id]

  #enable_deletion_protection = true

  tags = {
    Name = "bieber-lb"
  }
}
resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "back_end" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = aws_acm_certificate.cert.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.back_end.arn
  }
  depends_on = [
    aws_acm_certificate.cert,
    aws_lb_target_group.back_end
  ]
}

resource "aws_lb_target_group" "back_end" {  
  name     = "back-end-group"
  port     = 3001
  protocol = "HTTP"  
  vpc_id   = aws_vpc.vpc.id
} 

