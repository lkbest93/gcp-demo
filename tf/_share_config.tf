terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.5.0"
    }
  }
  backend "gcs" {
    bucket = "tfstate-bieber"
    prefix = "terraform/state"
  }
}
provider "aws" {region = "ap-northeast-2"} 
provider "google" {
  project = "kubernetes-316400" 
  region  = "asia-northeast3"
  zone    = "asia-northeast3-a"
}
